import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.cucumber.api.java.pt*;

public class Desconto {

    Funcionalidade: Desconto Compra com Dinheiro
    Como comprador
    Eu quero receber descontos
    Quando eu realizar o pagamento das minhas compras com dinheiro

    Cenário: Compra com Dinheiro - Com Desconto # Desconto.feature:9
    Dado que eu compre um bolo
    E realizar o pagamento
    Quando realizar o pagamento com dinheiro
    Então recebo 10% de desconto

    You can implement missing steps with the snippets below:

    Dado("^que eu compre um bolo$")
    public void que_eu_compre_um_bolo() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    Dado("^realizar o pagamento$")
    public void realizar_o_pagamento() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    Quando("^realizar o pagamento com dinheiro$")
    public void realizar_o_pagamento_com_dinheiro() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    Então("^recebo (\\d+)% de desconto$")
    public void recebo_de_desconto(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}